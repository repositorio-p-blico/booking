import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ChotelTestModule } from '../../../test.module';
import { HabUpdateComponent } from 'app/entities/hab/hab-update.component';
import { HabService } from 'app/entities/hab/hab.service';
import { Hab } from 'app/shared/model/hab.model';

describe('Component Tests', () => {
  describe('Hab Management Update Component', () => {
    let comp: HabUpdateComponent;
    let fixture: ComponentFixture<HabUpdateComponent>;
    let service: HabService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ChotelTestModule],
        declarations: [HabUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(HabUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HabUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HabService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Hab(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Hab();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
