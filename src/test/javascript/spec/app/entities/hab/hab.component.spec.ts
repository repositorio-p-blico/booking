import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { ChotelTestModule } from '../../../test.module';
import { HabComponent } from 'app/entities/hab/hab.component';
import { HabService } from 'app/entities/hab/hab.service';
import { Hab } from 'app/shared/model/hab.model';

describe('Component Tests', () => {
  describe('Hab Management Component', () => {
    let comp: HabComponent;
    let fixture: ComponentFixture<HabComponent>;
    let service: HabService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ChotelTestModule],
        declarations: [HabComponent],
      })
        .overrideTemplate(HabComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HabComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HabService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Hab(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.habs && comp.habs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
