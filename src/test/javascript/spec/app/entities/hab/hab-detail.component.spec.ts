import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChotelTestModule } from '../../../test.module';
import { HabDetailComponent } from 'app/entities/hab/hab-detail.component';
import { Hab } from 'app/shared/model/hab.model';

describe('Component Tests', () => {
  describe('Hab Management Detail Component', () => {
    let comp: HabDetailComponent;
    let fixture: ComponentFixture<HabDetailComponent>;
    const route = ({ data: of({ hab: new Hab(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ChotelTestModule],
        declarations: [HabDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(HabDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HabDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load hab on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.hab).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
