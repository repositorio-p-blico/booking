package com.mycompany.myapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mycompany.myapp.web.rest.TestUtil;

public class HabTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Hab.class);
        Hab hab1 = new Hab();
        hab1.setId(1L);
        Hab hab2 = new Hab();
        hab2.setId(hab1.getId());
        assertThat(hab1).isEqualTo(hab2);
        hab2.setId(2L);
        assertThat(hab1).isNotEqualTo(hab2);
        hab1.setId(null);
        assertThat(hab1).isNotEqualTo(hab2);
    }
}
