package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.ChotelApp;
import com.mycompany.myapp.domain.Hab;
import com.mycompany.myapp.repository.HabRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.domain.enumeration.Categoria;
/**
 * Integration tests for the {@link HabResource} REST controller.
 */
@SpringBootTest(classes = ChotelApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class HabResourceIT {

    private static final Integer DEFAULT_NUMBER_ROOM = 1;
    private static final Integer UPDATED_NUMBER_ROOM = 2;

    private static final Integer DEFAULT_BEDS = 1;
    private static final Integer UPDATED_BEDS = 2;

    private static final Boolean DEFAULT_AVAILABILITY = false;
    private static final Boolean UPDATED_AVAILABILITY = true;

    private static final Categoria DEFAULT_CATEGORY = Categoria.FAMILIAR;
    private static final Categoria UPDATED_CATEGORY = Categoria.DOBLE;

    @Autowired
    private HabRepository habRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHabMockMvc;

    private Hab hab;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hab createEntity(EntityManager em) {
        Hab hab = new Hab()
            .numberRoom(DEFAULT_NUMBER_ROOM)
            .beds(DEFAULT_BEDS)
            .availability(DEFAULT_AVAILABILITY)
            .category(DEFAULT_CATEGORY);
        return hab;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hab createUpdatedEntity(EntityManager em) {
        Hab hab = new Hab()
            .numberRoom(UPDATED_NUMBER_ROOM)
            .beds(UPDATED_BEDS)
            .availability(UPDATED_AVAILABILITY)
            .category(UPDATED_CATEGORY);
        return hab;
    }

    @BeforeEach
    public void initTest() {
        hab = createEntity(em);
    }

    @Test
    @Transactional
    public void createHab() throws Exception {
        int databaseSizeBeforeCreate = habRepository.findAll().size();
        // Create the Hab
        restHabMockMvc.perform(post("/api/habs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hab)))
            .andExpect(status().isCreated());

        // Validate the Hab in the database
        List<Hab> habList = habRepository.findAll();
        assertThat(habList).hasSize(databaseSizeBeforeCreate + 1);
        Hab testHab = habList.get(habList.size() - 1);
        assertThat(testHab.getNumberRoom()).isEqualTo(DEFAULT_NUMBER_ROOM);
        assertThat(testHab.getBeds()).isEqualTo(DEFAULT_BEDS);
        assertThat(testHab.isAvailability()).isEqualTo(DEFAULT_AVAILABILITY);
        assertThat(testHab.getCategory()).isEqualTo(DEFAULT_CATEGORY);
    }

    @Test
    @Transactional
    public void createHabWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = habRepository.findAll().size();

        // Create the Hab with an existing ID
        hab.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHabMockMvc.perform(post("/api/habs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hab)))
            .andExpect(status().isBadRequest());

        // Validate the Hab in the database
        List<Hab> habList = habRepository.findAll();
        assertThat(habList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllHabs() throws Exception {
        // Initialize the database
        habRepository.saveAndFlush(hab);

        // Get all the habList
        restHabMockMvc.perform(get("/api/habs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hab.getId().intValue())))
            .andExpect(jsonPath("$.[*].numberRoom").value(hasItem(DEFAULT_NUMBER_ROOM)))
            .andExpect(jsonPath("$.[*].beds").value(hasItem(DEFAULT_BEDS)))
            .andExpect(jsonPath("$.[*].availability").value(hasItem(DEFAULT_AVAILABILITY.booleanValue())))
            .andExpect(jsonPath("$.[*].category").value(hasItem(DEFAULT_CATEGORY.toString())));
    }
    
    @Test
    @Transactional
    public void getHab() throws Exception {
        // Initialize the database
        habRepository.saveAndFlush(hab);

        // Get the hab
        restHabMockMvc.perform(get("/api/habs/{id}", hab.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(hab.getId().intValue()))
            .andExpect(jsonPath("$.numberRoom").value(DEFAULT_NUMBER_ROOM))
            .andExpect(jsonPath("$.beds").value(DEFAULT_BEDS))
            .andExpect(jsonPath("$.availability").value(DEFAULT_AVAILABILITY.booleanValue()))
            .andExpect(jsonPath("$.category").value(DEFAULT_CATEGORY.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingHab() throws Exception {
        // Get the hab
        restHabMockMvc.perform(get("/api/habs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHab() throws Exception {
        // Initialize the database
        habRepository.saveAndFlush(hab);

        int databaseSizeBeforeUpdate = habRepository.findAll().size();

        // Update the hab
        Hab updatedHab = habRepository.findById(hab.getId()).get();
        // Disconnect from session so that the updates on updatedHab are not directly saved in db
        em.detach(updatedHab);
        updatedHab
            .numberRoom(UPDATED_NUMBER_ROOM)
            .beds(UPDATED_BEDS)
            .availability(UPDATED_AVAILABILITY)
            .category(UPDATED_CATEGORY);

        restHabMockMvc.perform(put("/api/habs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedHab)))
            .andExpect(status().isOk());

        // Validate the Hab in the database
        List<Hab> habList = habRepository.findAll();
        assertThat(habList).hasSize(databaseSizeBeforeUpdate);
        Hab testHab = habList.get(habList.size() - 1);
        assertThat(testHab.getNumberRoom()).isEqualTo(UPDATED_NUMBER_ROOM);
        assertThat(testHab.getBeds()).isEqualTo(UPDATED_BEDS);
        assertThat(testHab.isAvailability()).isEqualTo(UPDATED_AVAILABILITY);
        assertThat(testHab.getCategory()).isEqualTo(UPDATED_CATEGORY);
    }

    @Test
    @Transactional
    public void updateNonExistingHab() throws Exception {
        int databaseSizeBeforeUpdate = habRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHabMockMvc.perform(put("/api/habs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(hab)))
            .andExpect(status().isBadRequest());

        // Validate the Hab in the database
        List<Hab> habList = habRepository.findAll();
        assertThat(habList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHab() throws Exception {
        // Initialize the database
        habRepository.saveAndFlush(hab);

        int databaseSizeBeforeDelete = habRepository.findAll().size();

        // Delete the hab
        restHabMockMvc.perform(delete("/api/habs/{id}", hab.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Hab> habList = habRepository.findAll();
        assertThat(habList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
