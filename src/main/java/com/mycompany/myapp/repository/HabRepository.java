package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Hab;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Hab entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HabRepository extends JpaRepository<Hab, Long> {
}
