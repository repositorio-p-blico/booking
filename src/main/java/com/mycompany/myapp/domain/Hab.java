package com.mycompany.myapp.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

import com.mycompany.myapp.domain.enumeration.Categoria;

/**
 * A Hab.
 */
@Entity
@Table(name = "hab")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Hab implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "number_room")
    private Integer numberRoom;

    @Column(name = "beds")
    private Integer beds;

    @Column(name = "availability")
    private Boolean availability;

    @Enumerated(EnumType.STRING)
    @Column(name = "category")
    private Categoria category;

    @OneToOne
    @JoinColumn(unique = true)
    private Booking booking;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumberRoom() {
        return numberRoom;
    }

    public Hab numberRoom(Integer numberRoom) {
        this.numberRoom = numberRoom;
        return this;
    }

    public void setNumberRoom(Integer numberRoom) {
        this.numberRoom = numberRoom;
    }

    public Integer getBeds() {
        return beds;
    }

    public Hab beds(Integer beds) {
        this.beds = beds;
        return this;
    }

    public void setBeds(Integer beds) {
        this.beds = beds;
    }

    public Boolean isAvailability() {
        return availability;
    }

    public Hab availability(Boolean availability) {
        this.availability = availability;
        return this;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    public Categoria getCategory() {
        return category;
    }

    public Hab category(Categoria category) {
        this.category = category;
        return this;
    }

    public void setCategory(Categoria category) {
        this.category = category;
    }

    public Booking getBooking() {
        return booking;
    }

    public Hab booking(Booking booking) {
        this.booking = booking;
        return this;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Hab)) {
            return false;
        }
        return id != null && id.equals(((Hab) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Hab{" +
            "id=" + getId() +
            ", numberRoom=" + getNumberRoom() +
            ", beds=" + getBeds() +
            ", availability='" + isAvailability() + "'" +
            ", category='" + getCategory() + "'" +
            "}";
    }
}
