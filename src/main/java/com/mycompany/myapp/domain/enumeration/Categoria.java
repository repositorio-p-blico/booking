package com.mycompany.myapp.domain.enumeration;

/**
 * The Categoria enumeration.
 */
public enum Categoria {
    FAMILIAR, DOBLE, INDIVIDUAL
}
