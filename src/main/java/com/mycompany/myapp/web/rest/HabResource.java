package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Hab;
import com.mycompany.myapp.repository.HabRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Hab}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class HabResource {

    private final Logger log = LoggerFactory.getLogger(HabResource.class);

    private static final String ENTITY_NAME = "hab";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HabRepository habRepository;

    public HabResource(HabRepository habRepository) {
        this.habRepository = habRepository;
    }

    /**
     * {@code POST  /habs} : Create a new hab.
     *
     * @param hab the hab to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hab, or with status {@code 400 (Bad Request)} if the hab has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/habs")
    public ResponseEntity<Hab> createHab(@RequestBody Hab hab) throws URISyntaxException {
        log.debug("REST request to save Hab : {}", hab);
        if (hab.getId() != null) {
            throw new BadRequestAlertException("A new hab cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Hab result = habRepository.save(hab);
        return ResponseEntity.created(new URI("/api/habs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /habs} : Updates an existing hab.
     *
     * @param hab the hab to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hab,
     * or with status {@code 400 (Bad Request)} if the hab is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hab couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/habs")
    public ResponseEntity<Hab> updateHab(@RequestBody Hab hab) throws URISyntaxException {
        log.debug("REST request to update Hab : {}", hab);
        if (hab.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Hab result = habRepository.save(hab);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hab.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /habs} : get all the habs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of habs in body.
     */
    @GetMapping("/habs")
    public List<Hab> getAllHabs() {
        log.debug("REST request to get all Habs");
        return habRepository.findAll();
    }

    /**
     * {@code GET  /habs/:id} : get the "id" hab.
     *
     * @param id the id of the hab to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hab, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/habs/{id}")
    public ResponseEntity<Hab> getHab(@PathVariable Long id) {
        log.debug("REST request to get Hab : {}", id);
        Optional<Hab> hab = habRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(hab);
    }

    /**
     * {@code DELETE  /habs/:id} : delete the "id" hab.
     *
     * @param id the id of the hab to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/habs/{id}")
    public ResponseEntity<Void> deleteHab(@PathVariable Long id) {
        log.debug("REST request to delete Hab : {}", id);
        habRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
