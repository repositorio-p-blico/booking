import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ChotelSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [ChotelSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent],
})
export class ChotelHomeModule {}
