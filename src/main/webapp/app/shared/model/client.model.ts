export interface IClient {
  id?: number;
  name?: string;
  secondName?: string;
  cc?: number;
  phone?: number;
}

export class Client implements IClient {
  constructor(public id?: number, public name?: string, public secondName?: string, public cc?: number, public phone?: number) {}
}
