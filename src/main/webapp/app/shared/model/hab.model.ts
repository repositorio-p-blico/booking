import { IBooking } from 'app/shared/model/booking.model';
import { Categoria } from 'app/shared/model/enumerations/categoria.model';

export interface IHab {
  id?: number;
  numberRoom?: number;
  beds?: number;
  availability?: boolean;
  category?: Categoria;
  booking?: IBooking;
}

export class Hab implements IHab {
  constructor(
    public id?: number,
    public numberRoom?: number,
    public beds?: number,
    public availability?: boolean,
    public category?: Categoria,
    public booking?: IBooking
  ) {
    this.availability = this.availability || false;
  }
}
