import { Moment } from 'moment';
import { IClient } from 'app/shared/model/client.model';

export interface IBooking {
  id?: number;
  checking?: Moment;
  checkout?: Moment;
  client?: IClient;
}

export class Booking implements IBooking {
  constructor(public id?: number, public checking?: Moment, public checkout?: Moment, public client?: IClient) {}
}
