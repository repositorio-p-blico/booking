import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHab } from 'app/shared/model/hab.model';
import { HabService } from './hab.service';

@Component({
  templateUrl: './hab-delete-dialog.component.html',
})
export class HabDeleteDialogComponent {
  hab?: IHab;

  constructor(protected habService: HabService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.habService.delete(id).subscribe(() => {
      this.eventManager.broadcast('habListModification');
      this.activeModal.close();
    });
  }
}
