import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHab } from 'app/shared/model/hab.model';

@Component({
  selector: 'jhi-hab-detail',
  templateUrl: './hab-detail.component.html',
})
export class HabDetailComponent implements OnInit {
  hab: IHab | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ hab }) => (this.hab = hab));
  }

  previousState(): void {
    window.history.back();
  }
}
