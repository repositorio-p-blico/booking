import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IHab } from 'app/shared/model/hab.model';

type EntityResponseType = HttpResponse<IHab>;
type EntityArrayResponseType = HttpResponse<IHab[]>;

@Injectable({ providedIn: 'root' })
export class HabService {
  public resourceUrl = SERVER_API_URL + 'api/habs';

  constructor(protected http: HttpClient) {}

  create(hab: IHab): Observable<EntityResponseType> {
    return this.http.post<IHab>(this.resourceUrl, hab, { observe: 'response' });
  }

  update(hab: IHab): Observable<EntityResponseType> {
    return this.http.put<IHab>(this.resourceUrl, hab, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IHab>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IHab[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
