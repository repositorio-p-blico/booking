import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IHab, Hab } from 'app/shared/model/hab.model';
import { HabService } from './hab.service';
import { IBooking } from 'app/shared/model/booking.model';
import { BookingService } from 'app/entities/booking/booking.service';

@Component({
  selector: 'jhi-hab-update',
  templateUrl: './hab-update.component.html',
})
export class HabUpdateComponent implements OnInit {
  isSaving = false;
  bookings: IBooking[] = [];

  editForm = this.fb.group({
    id: [],
    numberRoom: [],
    beds: [],
    availability: [],
    category: [],
    booking: [],
  });

  constructor(
    protected habService: HabService,
    protected bookingService: BookingService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ hab }) => {
      this.updateForm(hab);

      this.bookingService
        .query({ filter: 'hab-is-null' })
        .pipe(
          map((res: HttpResponse<IBooking[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IBooking[]) => {
          if (!hab.booking || !hab.booking.id) {
            this.bookings = resBody;
          } else {
            this.bookingService
              .find(hab.booking.id)
              .pipe(
                map((subRes: HttpResponse<IBooking>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IBooking[]) => (this.bookings = concatRes));
          }
        });
    });
  }

  updateForm(hab: IHab): void {
    this.editForm.patchValue({
      id: hab.id,
      numberRoom: hab.numberRoom,
      beds: hab.beds,
      availability: hab.availability,
      category: hab.category,
      booking: hab.booking,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const hab = this.createFromForm();
    if (hab.id !== undefined) {
      this.subscribeToSaveResponse(this.habService.update(hab));
    } else {
      this.subscribeToSaveResponse(this.habService.create(hab));
    }
  }

  private createFromForm(): IHab {
    return {
      ...new Hab(),
      id: this.editForm.get(['id'])!.value,
      numberRoom: this.editForm.get(['numberRoom'])!.value,
      beds: this.editForm.get(['beds'])!.value,
      availability: this.editForm.get(['availability'])!.value,
      category: this.editForm.get(['category'])!.value,
      booking: this.editForm.get(['booking'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHab>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IBooking): any {
    return item.id;
  }
}
