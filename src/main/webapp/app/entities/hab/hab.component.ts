import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IHab } from 'app/shared/model/hab.model';
import { HabService } from './hab.service';
import { HabDeleteDialogComponent } from './hab-delete-dialog.component';

@Component({
  selector: 'jhi-hab',
  templateUrl: './hab.component.html',
})
export class HabComponent implements OnInit, OnDestroy {
  habs?: IHab[];
  eventSubscriber?: Subscription;

  constructor(protected habService: HabService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.habService.query().subscribe((res: HttpResponse<IHab[]>) => (this.habs = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInHabs();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IHab): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInHabs(): void {
    this.eventSubscriber = this.eventManager.subscribe('habListModification', () => this.loadAll());
  }

  delete(hab: IHab): void {
    const modalRef = this.modalService.open(HabDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.hab = hab;
  }
}
