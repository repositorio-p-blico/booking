import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ChotelSharedModule } from 'app/shared/shared.module';
import { HabComponent } from './hab.component';
import { HabDetailComponent } from './hab-detail.component';
import { HabUpdateComponent } from './hab-update.component';
import { HabDeleteDialogComponent } from './hab-delete-dialog.component';
import { habRoute } from './hab.route';

@NgModule({
  imports: [ChotelSharedModule, RouterModule.forChild(habRoute)],
  declarations: [HabComponent, HabDetailComponent, HabUpdateComponent, HabDeleteDialogComponent],
  entryComponents: [HabDeleteDialogComponent],
})
export class ChotelHabModule {}
