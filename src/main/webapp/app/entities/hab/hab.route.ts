import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IHab, Hab } from 'app/shared/model/hab.model';
import { HabService } from './hab.service';
import { HabComponent } from './hab.component';
import { HabDetailComponent } from './hab-detail.component';
import { HabUpdateComponent } from './hab-update.component';

@Injectable({ providedIn: 'root' })
export class HabResolve implements Resolve<IHab> {
  constructor(private service: HabService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IHab> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((hab: HttpResponse<Hab>) => {
          if (hab.body) {
            return of(hab.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Hab());
  }
}

export const habRoute: Routes = [
  {
    path: '',
    component: HabComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'chotelApp.hab.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: HabDetailComponent,
    resolve: {
      hab: HabResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'chotelApp.hab.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: HabUpdateComponent,
    resolve: {
      hab: HabResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'chotelApp.hab.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: HabUpdateComponent,
    resolve: {
      hab: HabResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'chotelApp.hab.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
