import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'booking',
        loadChildren: () => import('./booking/booking.module').then(m => m.ChotelBookingModule),
      },
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.ChotelClientModule),
      },
      {
        path: 'hab',
        loadChildren: () => import('./hab/hab.module').then(m => m.ChotelHabModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class ChotelEntityModule {}
